<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace codesk\likeaboss;

use yii\web\AssetBundle;

/**
 * This declares the asset files required by Likeaboss.
 *
 * @author Surachet Munchawanon <sukung34@gmail.com>
 * @since 2.0
 */
class LikeabossAsset extends AssetBundle {

    /**
     * @inheritdoc
     */
    public $sourcePath = '@codesk/likeaboss/assets';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/likeaboss.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'js/likeaboss.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}
